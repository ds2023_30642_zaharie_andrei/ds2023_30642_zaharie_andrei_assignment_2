package com.example.userservice.Configurations;

import com.example.userservice.Controllers.Models.RegisterRequest;
import com.example.userservice.Controllers.Models.UserRequest;
import com.example.userservice.Controllers.Models.UserResponse;
import com.example.userservice.Repository.Models.User;
import com.example.userservice.Services.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@RequiredArgsConstructor
public class UserConfigurations {

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.addMappings(new PropertyMap<User, UserRequest>() {
            @Override
            protected void configure() {
                map(source.getEmail(), destination.getEmail());
                map(source.getPassword(), destination.getPassword());
                map(source.getRole(), destination.getRole());
                map(source.getGuid(), destination.getGuid());
            }
        });
        modelMapper.addMappings(new PropertyMap<UserRequest, User>() {
            @Override
            protected void configure() {
                map(source.getEmail(), destination.getEmail());
                map(source.getPassword(), destination.getPassword());
                map(source.getRole(), destination.getRole());
                map(source.getGuid(), destination.getGuid());
            }
        });
        modelMapper.addMappings(new PropertyMap<User, UserResponse>() {
            @Override
            protected void configure() {
                map(source.getEmail(), destination.getEmail());
                map(source.getRole(), destination.getRole());
                map(source.getGuid(), destination.getGuid());
            }
        });
        modelMapper.addMappings(new PropertyMap<RegisterRequest, User>() {
            @Override
            protected void configure() {
                map(source.getEmail(), destination.getEmail());
                map(source.getName(), destination.getName());
                map(source.getPassword(), destination.getPassword());
                map(source.getRole(), destination.getRole());
            }
        });
        return modelMapper;
    }
}