package com.example.userservice.Controllers.Models;


import lombok.Data;

@Data
public class UserRequest {
    private String guid;
    private String email;
    private String name;
    private String password;
    private String role;
}
