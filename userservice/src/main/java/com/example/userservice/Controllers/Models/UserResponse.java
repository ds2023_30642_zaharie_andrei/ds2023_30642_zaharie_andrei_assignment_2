package com.example.userservice.Controllers.Models;

import lombok.Data;

@Data
public class UserResponse {
    private String guid;
    private String email;
    private String name;
    private String role;
}
