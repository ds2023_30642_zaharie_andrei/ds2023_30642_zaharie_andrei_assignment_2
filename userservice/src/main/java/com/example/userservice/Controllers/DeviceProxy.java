package com.example.userservice.Controllers;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name="deviceControllerProxy", url="http://172.16.200.5:8081")
public interface DeviceProxy {
    @PostMapping("/api/devices/deleteUserDevices/{userGuid}")
    ResponseEntity<?> deleteUserDevices(@PathVariable String userGuid);
}
