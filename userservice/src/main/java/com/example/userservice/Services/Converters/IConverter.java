package com.example.userservice.Services.Converters;

public interface IConverter<T1, T2> {
    T2 toDto(T1 model);

    T1 toModel(T2 dto);
}
