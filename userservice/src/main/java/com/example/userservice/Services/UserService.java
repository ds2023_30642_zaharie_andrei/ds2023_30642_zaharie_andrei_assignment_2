package com.example.userservice.Services;

import com.example.userservice.Controllers.Models.UserRequest;
import com.example.userservice.Controllers.Models.UserResponse;
import com.example.userservice.Repository.Models.User;
import com.example.userservice.Repository.IUserRepository;
import com.example.userservice.Services.Converters.UserRequestConverter;
import com.example.userservice.Services.Converters.UserResponseConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor

public class UserService implements IUserService {
    private final IUserRepository userRepository;
    private final UserResponseConverter userResponseConverter;
    private final UserRequestConverter userRequestConverter;

    @Override
    public UserResponse save(UserRequest user) {
        User userModel = userRequestConverter.toModel(user);

        return userResponseConverter.toDto(
                userRepository.save(userModel)
        );
    }
    @Override
    public User findByEmail(String email) {
        User userModel = userRepository.findUserByEmail(email).orElseThrow(
                ()-> new RuntimeException("Cannot find the user with the email: " + email));

        return userModel;
    }

    @Override
    public List<UserResponse> findAll() {
        List<User> users = userRepository.findAll();

        return users.stream()
                .map(userResponseConverter::toDto)
                .toList();
    }

    @Override
    public UserResponse find(String guid) {
        User userModel = userRepository.findUserByGuid(guid).orElse(null);

        if(userModel == null) return null;

        return userResponseConverter.toDto(userModel);
    }

    @Override
    public void delete(String guid) {
        User deleteUser = userRepository.findUserByGuid(guid).orElseThrow(
                ()-> new RuntimeException("Cannot delete the user with the id: " + guid));
        if(deleteUser != null) {
            userRepository.delete(deleteUser);
        }
    }

    @Override
    public UserResponse update(String guid, UserRequest userRequest) {
        User user = userRepository.findUserByGuid(guid).orElseThrow(
                ()-> new RuntimeException("Cannot update the user with the guid: " + guid));
        user.setEmail(userRequest.getEmail());
        user.setName(userRequest.getName());
        user.setRole(userRequest.getRole());

        return userResponseConverter.toDto(userRepository.save(user));
    }



}

