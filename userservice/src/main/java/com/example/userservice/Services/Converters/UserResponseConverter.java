package com.example.userservice.Services.Converters;

import com.example.userservice.Controllers.Models.UserResponse;
import com.example.userservice.Repository.Models.User;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor

public class UserResponseConverter implements IConverter<User, UserResponse>{
    private final ModelMapper mapper;
    @Override
    public UserResponse toDto(User model) {
        return mapper.map(model, UserResponse.class);
    }

    @Override
    public User toModel(UserResponse dto) {
        return mapper.map(dto, User.class);
    }
}
