package com.example.userservice.Services.Converters;

import com.example.userservice.Controllers.Models.RegisterRequest;
import com.example.userservice.Controllers.Models.UserRequest;
import com.example.userservice.Repository.Models.User;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserRegisterConvertors implements IConverter<User, RegisterRequest>{
    private final ModelMapper mapper;

    @Override
    public RegisterRequest toDto(User model) {
        return mapper.map(model, RegisterRequest.class);
    }

    @Override
    public User toModel(RegisterRequest dto) {
        return mapper.map(dto, User.class);
    }
}
