package com.example.userservice.Services.Converters;

import com.example.userservice.Controllers.Models.UserRequest;
import com.example.userservice.Controllers.Models.UserResponse;
import com.example.userservice.Repository.Models.User;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor

public class UserRequestConverter implements IConverter<User, UserRequest>{
    private final ModelMapper mapper;
    @Override
    public UserRequest toDto(User model) {
        return mapper.map(model, UserRequest.class);
    }

    @Override
    public User toModel(UserRequest dto) {
        return mapper.map(dto, User.class);
    }
}
