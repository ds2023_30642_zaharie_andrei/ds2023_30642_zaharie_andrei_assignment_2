package com.example.userservice.Services.Security;

import com.example.userservice.Repository.Models.User;

import java.util.Map;

public interface IJwtService {
    String extractUserEmail(String token);

    String generateToken(User userDetails);

    boolean isTokenValid(String token, User userDetails);

}
