package com.example.monitoringservice.Repository;

import com.example.monitoringservice.Repository.Model.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface IDeviceRepository extends JpaRepository<Device, Long> {
    @Query("SELECT d FROM Device d WHERE d.guid = ?1")
    Optional<Device> findDeviceByGuid(String guid);
}
