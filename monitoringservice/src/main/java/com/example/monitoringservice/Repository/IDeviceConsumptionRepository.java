package com.example.monitoringservice.Repository;

import com.example.monitoringservice.Repository.Model.DeviceConsumption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IDeviceConsumptionRepository extends JpaRepository<DeviceConsumption, Long> {

}
