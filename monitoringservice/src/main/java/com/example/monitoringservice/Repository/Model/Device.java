package com.example.monitoringservice.Repository.Model;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "device")
public class Device {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private long id;
    @Column(name = "deviceGuid")
    private String guid;
    @Column(name = "max_consumption")
    private float maxConsumption;
    @Column(name = "ownerGuid")
    private String ownerGuid;

    public Device(){}
    public Device(String guid, String ownerGuid, float maxConsumption){
        this.guid = guid;
        this.ownerGuid = ownerGuid;
        this.maxConsumption = maxConsumption;
    }
}