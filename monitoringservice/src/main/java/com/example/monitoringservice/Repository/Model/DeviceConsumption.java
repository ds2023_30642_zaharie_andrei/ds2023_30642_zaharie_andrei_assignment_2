package com.example.monitoringservice.Repository.Model;


import jakarta.persistence.*;
import lombok.Getter;

@Entity
@Getter
@Table(name = "consumption")
public class DeviceConsumption {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private long id;
    @Column(name = "device_guid")
    private String guid;
    @Column(name = "consumption")
    private float energyConsumption;
    @Column(name = "timestamp")
    private long timestamp;

    public DeviceConsumption(){}

    public DeviceConsumption( String guid, float energyConsumption, long timestamp){
        this.guid = guid;
        this.energyConsumption = energyConsumption;
        this.timestamp = timestamp;
    }

}
