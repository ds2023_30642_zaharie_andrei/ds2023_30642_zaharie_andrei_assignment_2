package com.example.monitoringservice.Services;


import com.example.monitoringservice.Models.DeviceEnergyConsumption;
import com.example.monitoringservice.Repository.IDeviceConsumptionRepository;
import com.example.monitoringservice.Repository.Model.DeviceConsumption;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DeviceConsumptionService {
    private final IDeviceConsumptionRepository consumptionRepository;
    public void save(DeviceEnergyConsumption deviceEnergyConsumption){
        DeviceConsumption device = convertModel(deviceEnergyConsumption);

        DeviceConsumption savedDevice = this.consumptionRepository.save(device);
        if(savedDevice != null){
            System.out.println("[" +savedDevice.getId() + "]Device with guid: " + savedDevice.getGuid() + " saved in db.");
        }
    }
    public List<DeviceConsumption> getDeviceEnergyConsumption(String deviceGuid){
        List<DeviceConsumption> devices = consumptionRepository.findAll();

        return devices.stream().filter(d -> d.getGuid().equals(deviceGuid)).toList();
    }
    private DeviceConsumption convertModel(DeviceEnergyConsumption deviceEnergyConsumption){
        return new DeviceConsumption(
                deviceEnergyConsumption.getDeviceId(),
                deviceEnergyConsumption.getEnergyConsumption(),
                deviceEnergyConsumption.getTimestamp()
        );
    }
}
