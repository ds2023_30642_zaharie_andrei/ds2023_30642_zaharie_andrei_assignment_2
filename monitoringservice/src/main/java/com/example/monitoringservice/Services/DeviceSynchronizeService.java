package com.example.monitoringservice.Services;

import com.example.monitoringservice.Models.DeviceSynchronize;
import com.example.monitoringservice.Repository.IDeviceRepository;
import com.example.monitoringservice.Repository.Model.Device;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DeviceSynchronizeService {
    private final IDeviceRepository deviceRepository;
    public void save(DeviceSynchronize syncedDevice){
        Device device = convertModel(syncedDevice);

        Device savedDevice = this.deviceRepository.save(device);
        if(savedDevice != null){
            System.out.println("Sync - [" +savedDevice.getId() + "]Device with guid: " + savedDevice.getGuid() + " saved in db.");
        }
    }

    public List<Device> getUserDevices(String userGuid){
        List<Device> userDevices = this.deviceRepository.findAll();

        return userDevices.stream().filter(d ->
        {
            String ownerGuid =  d.getOwnerGuid();
            return ownerGuid != null && ownerGuid.equals(userGuid);
        }).toList();
    }

    public List<Device> getAll(){
        return this.deviceRepository.findAll();
    }

    public void update(DeviceSynchronize syncedDevice){
        Device device = convertModel(syncedDevice);
        Device updateDevice = this.deviceRepository.findDeviceByGuid(syncedDevice.getDeviceGuid()).orElse(null);

        if(updateDevice != null) {
            updateDevice.setGuid(device.getGuid());
            updateDevice.setMaxConsumption(device.getMaxConsumption());
            updateDevice.setOwnerGuid(device.getOwnerGuid());
        }else{
            updateDevice = device;
        }
        Device savedDevice = this.deviceRepository.save(updateDevice);
        if(savedDevice != null){
            System.out.println("Sync - [" +savedDevice.getId() + "]Device with guid: " + savedDevice.getGuid() + " updated in db.");
        }
    }

    public void delete(DeviceSynchronize syncedDevice){
        Device deleteDevice = this.deviceRepository.findDeviceByGuid(syncedDevice.getDeviceGuid()).orElse(null);

        if(deleteDevice != null){
            this.deviceRepository.delete(deleteDevice);
            System.out.println("Sync - [" +deleteDevice.getId() + "]Device with guid: " + deleteDevice.getGuid() + " deleted from db.");
        }
    }

    private Device convertModel(DeviceSynchronize syncedDevice){
        return new Device(
                syncedDevice.getDeviceGuid(),
                syncedDevice.getOwnerGuid(),
                syncedDevice.getMaxEnergyConsumption()
        );
    }

}
