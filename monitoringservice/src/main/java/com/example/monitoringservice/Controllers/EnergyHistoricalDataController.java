package com.example.monitoringservice.Controllers;

import com.example.monitoringservice.Repository.Model.DeviceConsumption;
import com.example.monitoringservice.Services.DeviceConsumptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/api")
@RequiredArgsConstructor
@CrossOrigin("http://localhost:3000")
public class EnergyHistoricalDataController {
    private final DeviceConsumptionService deviceConsumptionService;

    @GetMapping("/energyConsumption/{guid}")
    public ResponseEntity<?> getEnergyConsumptions(@PathVariable String guid){
        List<DeviceConsumption> energyConsumption = deviceConsumptionService.getDeviceEnergyConsumption(guid);

        return ResponseEntity.ok(energyConsumption);
    }
}