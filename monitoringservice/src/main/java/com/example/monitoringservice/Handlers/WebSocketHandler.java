package com.example.monitoringservice.Handlers;


import com.example.monitoringservice.Models.DeviceEnergyConsumption;
import com.example.monitoringservice.Models.EnergyConsumptionMessage;
import com.example.monitoringservice.Repository.Model.Device;
import com.example.monitoringservice.Services.DeviceSynchronizeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

@Controller
@RequiredArgsConstructor
@RequestMapping("/sockets")
@CrossOrigin(origins = "http://localhost:3000")
public class WebSocketHandler {

    final SimpMessagingTemplate simpMessageTemplate;
    private static HashMap<String, String> connectedUsers = new HashMap<String, String>();
    final DeviceSynchronizeService deviceSynchronizeService;
    @GetMapping("/get")
    public ResponseEntity<?> messgTest(){

        connectedUsers.put("61ede254-c017-42c6-9aac-c8302d1d8833","c8a9c60e-93f6-4f46-82fa-cc95073a55f9");

        EnergyConsumptionMessage message = new EnergyConsumptionMessage();
        message.setCurrentConsumption(100);
        message.setDeviceGuid("61ede254-c017-42c6-9aac-c8302d1d8833");
        message.setUserGuid("c8a9c60e-93f6-4f46-82fa-cc95073a55f9");
        message.setTimeStamp(LocalDate.now().toString());

        sendMessageToUser(message, "c8a9c60e-93f6-4f46-82fa-cc95073a55f9");
        return ResponseEntity.ok(message);
    }

    public void sendMessageToOwnerDevice(DeviceEnergyConsumption device){
        String ownerGuid = connectedUsers.get(device.getDeviceId());

        if(ownerGuid == null) return;

        EnergyConsumptionMessage message = new EnergyConsumptionMessage();
        message.setDeviceGuid(device.getDeviceId());
        message.setUserGuid(ownerGuid);
        message.setCurrentConsumption(device.getEnergyConsumption());

        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm");
        Date resultdate = new Date(device.getTimestamp());

        message.setTimeStamp(sdf.format(resultdate));

        sendMessageToUser(message, ownerGuid);
    }

    @MessageMapping("/send-message/{userGuid}")
    public void sendMessageToUser(@Payload EnergyConsumptionMessage message, @DestinationVariable("userGuid") String userGuid) {
        if(!checkConnection(userGuid)) return;

        simpMessageTemplate.convertAndSendToUser(userGuid, "/message", message);
    }

    @MessageMapping("/connect/{userGuid}")
    public void handleUserConnect(@Payload String message,@DestinationVariable("userGuid") String userGuid) {
        System.out.println("User connected: " + userGuid + " Message: " + message);
        List<Device> userDevices = deviceSynchronizeService.getUserDevices(userGuid);

        for(Device device: userDevices){
            connectedUsers.put(device.getGuid(), device.getOwnerGuid());
        }

    }

    @MessageMapping("/disconnect/{userGuid}")
    public void handleUserDisconnect(@DestinationVariable("userGuid") String userGuid) {
        System.out.println("User disconnected: " + userGuid);

        Iterator<Map.Entry<String, String>> iterator = connectedUsers.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, String> entry = iterator.next();
            if (entry.getValue().equals(userGuid)) {
                iterator.remove();
            }
        }
    }

    public void updateConnectedUsers(){
        //merg prin lista de device si useri si o syncronizez cu modificarile anterioare.
        for(String key : connectedUsers.keySet()){
            String userGuid = connectedUsers.get(key);
            List<Device> devices =  deviceSynchronizeService.getUserDevices(userGuid);
            for(Device device : devices){
                connectedUsers.put(device.getGuid(), userGuid);
            }
            if(devices.isEmpty()){
                connectedUsers.remove(key);
            }
        }
    }

    private boolean checkConnection(String userguid){
        return connectedUsers.containsValue(userguid);
    }
}
