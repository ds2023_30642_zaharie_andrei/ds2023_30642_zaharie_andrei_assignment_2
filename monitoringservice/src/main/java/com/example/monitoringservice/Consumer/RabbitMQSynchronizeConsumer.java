package com.example.monitoringservice.Consumer;


import com.example.monitoringservice.Handlers.WebSocketHandler;
import com.example.monitoringservice.Models.DeviceSynchronize;
import com.example.monitoringservice.Services.DeviceSynchronizeService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RabbitMQSynchronizeConsumer {
    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQEnergyConsumer.class);
    private final DeviceSynchronizeService deviceSynchronizeService;
    private final WebSocketHandler webSocketHandler;

    @RabbitListener(queues = {"${rabbitmq.sync.queue.name}"})
    public void consume(DeviceSynchronize device){

        if(device == null) return;
        LOGGER.info("Massage received and consumed -> "+ device.toString());
        System.out.println("Massage received and consumed -> " + device);

        if(device.getSyncType().equals("delete")){
            deviceSynchronizeService.delete(device);
            return;
        }
        if(device.getSyncType().equals("removeOwner")){
            device.setOwnerGuid(null);
            deviceSynchronizeService.update(device);
            return;
        }
        if(device.getSyncType().equals("add")){
            deviceSynchronizeService.save(device);
            return;
        }
        deviceSynchronizeService.update(device);
        webSocketHandler.updateConnectedUsers();
    }


}
