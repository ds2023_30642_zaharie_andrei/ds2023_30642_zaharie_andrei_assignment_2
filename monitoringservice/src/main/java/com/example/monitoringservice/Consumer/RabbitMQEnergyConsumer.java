package com.example.monitoringservice.Consumer;


import com.example.monitoringservice.Handlers.WebSocketHandler;
import com.example.monitoringservice.Models.DeviceEnergyConsumption;
import com.example.monitoringservice.Models.HourlyConsumptionModel;
import com.example.monitoringservice.Services.DeviceConsumptionService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
@RequiredArgsConstructor
public class RabbitMQEnergyConsumer {
    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQEnergyConsumer.class);
    private final DeviceConsumptionService deviceConsumptionService;
    private final WebSocketHandler webSocketHandler;
    private static HashMap<String, HourlyConsumptionModel> timerMap = new HashMap<String, HourlyConsumptionModel>();
    @RabbitListener(queues = {"${rabbitmq.queue.name}"})
    public void consume(DeviceEnergyConsumption device){
        if(device == null) return;
        LOGGER.info("Massage received and consumed -> "+ device.toString());
        System.out.println("Massage received and consumed -> " + device);

        timerMap.computeIfAbsent(device.getDeviceId(), k -> new HourlyConsumptionModel(device.getTimestamp(),device.getEnergyConsumption()));

        webSocketHandler.sendMessageToOwnerDevice(device);

        if(checkOneHourDifference(device)) {
            device.setEnergyConsumption(device.getEnergyConsumption() - timerMap.get(device.getDeviceId()).getLastConsumption());
            timerMap.put(device.getDeviceId(), new HourlyConsumptionModel(device.getTimestamp(), device.getEnergyConsumption()));
            deviceConsumptionService.save(device);
        }

    }

    private Boolean checkOneHourDifference(DeviceEnergyConsumption device){
        long deviceTimestamp = device.getTimestamp();
        long timerTimestamp = timerMap.get(device.getDeviceId()).getTimestamp();

        return deviceTimestamp - timerTimestamp > 120000;
    }
}
