package com.example.monitoringservice.Models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Data
public class HourlyConsumptionModel {
    private long timestamp;
    private float lastConsumption;

    public HourlyConsumptionModel(long timestamp, float consumption){
        this.timestamp = timestamp;
        this.lastConsumption = consumption;
    }

}