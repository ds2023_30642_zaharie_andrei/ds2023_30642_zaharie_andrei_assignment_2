package com.example.monitoringservice.Models;

import lombok.Data;

import java.io.Serializable;

@Data
public class DeviceEnergyConsumption implements Serializable {
    private long timestamp;
    private String deviceId;
    private float energyConsumption;

    @Override
    public String toString(){
        return "Timestamp: "+ timestamp +"\nDeviceId: " + deviceId+ "\n Energy Consumption: "+energyConsumption+" kWh.";
    }
}
