package com.example.monitoringservice.Models;

import lombok.Data;

@Data
public class DeviceSynchronize {
    private String deviceGuid;

    private String ownerGuid;

    private float maxEnergyConsumption;

    private String syncType;
}
