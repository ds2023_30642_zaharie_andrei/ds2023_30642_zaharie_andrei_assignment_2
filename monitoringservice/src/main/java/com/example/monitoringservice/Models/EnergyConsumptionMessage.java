package com.example.monitoringservice.Models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class EnergyConsumptionMessage {
    private String userGuid;
    private String deviceGuid;
    private float currentConsumption;
    private String timeStamp;
}