import React, {useState, useEffect, useContext, useRef} from 'react';
import './ChatModal.css';
import Modal from "@mui/material/Modal";
import { User, ChatMessage } from "../users/User.types";
import {UserContext} from "../UserProvider";
import SockJS from "sockjs-client";
import {Stomp} from "@stomp/stompjs/esm6";
interface ChatModalProps {
    onClose: () => void;
    open: boolean;
    user: User | null;
    recievedMessage: ChatMessage |null;
}

interface UserChat {
    [key: string]: string[];
}
const ChatModal: React.FC<ChatModalProps> = ({open, onClose, user , recievedMessage}) => {
    const {chatClient, setChatClient, userGuid} = useContext(UserContext);
    const [inputValue, setInputValue] = useState('');
    // const [messages, setMessages] = useState<string[]>([]);
    const [isTyping, setIsTyping] = useState(false);
    const role = localStorage.getItem("role");
    const messagesEndRef = useRef<HTMLDivElement>(null);
    const [scroll, setScroll] = useState(0);
    const [chats, setChats] = useState<UserChat>({"start":[]});

    useEffect(() => {
        console.log("RECEIVED MESSAGE IN CHAT", recievedMessage);
        if(recievedMessage && recievedMessage.message != null){
            if(recievedMessage.type ==="typing"){
                setIsTyping(true)
            }
            else if(recievedMessage.type ==="message") {
                setIsTyping(false);
                if(role === "Admin") {
                    recieveMessage(recievedMessage.message, recievedMessage.sendFrom);
                }
                else{
                    recieveMessage(recievedMessage.message, user?.guid);
                }
            }else if(recievedMessage.type === "stoppedTyping"){
                setIsTyping(false);
            }
            scrollToBottom();
        }
    }, [recievedMessage]);


    useEffect(() => {
        scrollToBottom();
    }, [scroll]);

/*    useEffect(() => {
        if(user?.guid && chats[user?.guid]) {
            setMessages(chats[user?.guid]);
        }else if(user?.guid){
            chats[user.guid] = [];
            setMessages(chats[user.guid]);
        }
    }, []);*/
    function recieveMessage(message : string, userGuid: string | undefined){

        const newMessage = `<p id="received" class="messageParagraph">${message}</p>`;
        if (userGuid){
            addMessageToChat(userGuid, newMessage);
        }
        setInputValue('');
    }
    function addMessageToChat (userGuid: string, message: string) {
        setChats(prevChatHistory => ({
            ...prevChatHistory,
            [userGuid]: [...(prevChatHistory[userGuid] || []), message],
        }));
    };
    function sendTypingMessage(type: string){
        console.log("USER ROLEEE",role);
        if(role === "admin" && user?.guid){
            const sendMessage= {
                sendFrom: "Admin",
                sendTo: user.guid,
                message: " sal",
                type: type,
                timestamp:Date.now(),
            }
            if(chatClient) {
                chatClient.send(`/app/chat/${user?.guid}/message`, {}, JSON.stringify(sendMessage));
            }
        } else if(role === "user" && user?.guid) {
            const sendMessage= {
                sendFrom: user.guid,
                sendTo: "Admin",
                message: " sal",
                type: type,
                timestamp:Date.now(),
            }
            if (chatClient) {
                chatClient.send(`/app/chat/message/admin`, {}, JSON.stringify(sendMessage));
            }
        }
        scrollToBottom();
    }
    function sendTest(){
        const newMessage = `<p id="sent" class="messageParagraph">${inputValue}</p>`;

        if(role === "admin" && user?.guid){
            const sendMessage= {
                sendFrom: "Admin",
                sendTo: user.guid,
                message: inputValue,
                type: "message",
                timestamp:Date.now(),
            }
            if(chatClient) {
                chatClient.send(`/app/chat/${user?.guid}/message`, {}, JSON.stringify(sendMessage));
            }
            addMessageToChat(user.guid, newMessage)
        } else if(role === "user" && user?.guid) {
            const sendMessage= {
                sendFrom: user.guid,
                sendTo: "Admin",
                message: inputValue,
                type: "message",
                timestamp:Date.now(),
            }
            if (chatClient) {
                chatClient.send(`/app/chat/message/admin`, {}, JSON.stringify(sendMessage));
            }
            addMessageToChat(user.guid, newMessage)
        }

        setInputValue('');
        setScroll(scroll + 1 );
    }

    const scrollToBottom = () => {
        const messagesContent = document.querySelector(".messages-content");
        if (messagesContent) {
            messagesContent.scrollTop = messagesContent.scrollHeight;
        }
    };

    return (
        <Modal open={open} onClose={onClose}>
            <section className="avenue-messenger">
                <div className="agent-face">
                    <div className="half">
                        <img className="agent circle" src="/userIcon.jpg" alt="User" />
                    </div>
                </div>
                <div className="chat">
                    <div className="chat-title">
                        <h1>{user?.name}</h1>
                        <h2>{user?.email}</h2>
                    </div>
                    <div className="messages">
                        <div className="messages-content">
                            {user?.guid && chats[user.guid] && chats[user.guid].map((message, index) => {
                                const messageClass = message.startsWith('<p id="sent" class="messageParagraph">') ? 'message-sent' : 'message-received';
                                return (
                                    <div
                                        key={index}
                                        dangerouslySetInnerHTML={{ __html: message }}
                                        className={`${messageClass}`}
                                    ></div>
                                );
                            })}
                            <div ref={messagesEndRef} />
                        </div>
                    </div>
                    {isTyping &&
                        (
                            <div className="typing">typing...</div>
                        )
                    }
                    <div className="message-box">
                        <textarea
                            className="message-input"
                            placeholder="Type message..."
                            value={inputValue}
                            onChange={(e) => setInputValue(e.target.value)}
                            onFocusCapture={() => sendTypingMessage("typing")}
                            onBlur={() => sendTypingMessage("stoppedTyping")}
                        />
                        <button type="submit" className="message-submit" onClick={sendTest}>
                            Send
                        </button>
                    </div>
                </div>
            </section>
        </Modal>
    );
};
export default ChatModal;
