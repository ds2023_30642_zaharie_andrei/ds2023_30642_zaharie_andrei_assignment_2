export type IUserLogin = {
  email: string;
  password: string;
}

export type IUserRegisterRequest = {
  email: string;
  name: string;
  password: string;
  role: string;
}