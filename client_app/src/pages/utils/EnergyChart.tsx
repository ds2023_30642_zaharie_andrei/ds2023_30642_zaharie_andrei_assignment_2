import * as React from 'react';
import { BarPlot } from '@mui/x-charts/BarChart';
import { LinePlot } from '@mui/x-charts/LineChart';
import { ChartContainer } from '@mui/x-charts/ChartContainer';
import { AllSeriesType } from '@mui/x-charts/models';
import { ChartsXAxis } from '@mui/x-charts/ChartsXAxis';
import { ChartsYAxis } from '@mui/x-charts/ChartsYAxis';
import {DeviceConsumption} from "../users/User.types";

interface EnergyChartProps {
    deviceConsumptions: DeviceConsumption[];
    maxConsumption: number; // Include maxConsumption in props
}
const EnergyChart: React.FC<EnergyChartProps> = ({ deviceConsumptions, maxConsumption }) => {

    const convertTimestampToDate = (timestamp : number) => {
        const date = new Date(timestamp);
        const hours = date.getHours().toString().padStart(2, '0'); // Get hours with leading zero if needed
        const minutes = date.getMinutes().toString().padStart(2, '0'); // Get minutes with leading zero if needed
        return `${hours}:${minutes}`;
    };

    const series: AllSeriesType[] = [
        {
            type: 'bar',
            stack: '',
            yAxisKey: 'energy',
            data: deviceConsumptions.map((data) => data.energyConsumption),
        },
        {
            type: 'line',
            yAxisKey: 'energy_divided',
            color: 'red',
            data: deviceConsumptions.map((data) => data.energyConsumption / maxConsumption),
        },
    ];

    return (
        <ChartContainer
            series={series}
            width={500}
            height={400}
            xAxis={[
                {
                    id: 'timeStamp',
                    data: deviceConsumptions.map((data) => convertTimestampToDate(data.timestamp)),
                    scaleType: 'band',
                    valueFormatter: (value) => (value !== undefined ? value.toString() : ''),
                },
            ]}
            yAxis={[
                {
                    id: 'energy',
                    scaleType: 'linear',
                },
                {
                    id: 'energy_divided',
                    scaleType: 'linear',
                },
            ]}
        >
            <BarPlot />
            <LinePlot />
            <ChartsXAxis label="Time Stamp" position="bottom" axisId="timeStamp" />
            <ChartsYAxis
                label="Energy Consumption (kWh)"
                position="left"
                axisId="energy"
                tickFormatter={(value: number) => `${value} kWh`}
            />
            <ChartsYAxis
                label="Growing Speed"
                position="right"
                axisId="energy_divided"
                tickFormatter={(value: number) => `${value * 100} kWh`}
            />
        </ChartContainer>
    );
}
export default EnergyChart;