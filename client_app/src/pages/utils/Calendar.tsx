import * as React from 'react';
import { DemoContainer } from '@mui/x-date-pickers/internals/demo';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';

interface CalendarProps {
    onChange: (date: Date) => void; // Function to handle date change
}
const Calendar: React.FC<CalendarProps> = ({ onChange }) => {
    const handleDateChange = (date: Date | null) => {
        if(date != null) {
            date = new Date(date);
            console.log('Selected date:', date);
            onChange(date);
        }
    };

    return (
        <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DemoContainer components={['DatePicker']}>
                <DatePicker label="Consumption Day"
                            onChange={handleDateChange}
                            disableFuture />
            </DemoContainer>
        </LocalizationProvider>
    );
};

export default Calendar;