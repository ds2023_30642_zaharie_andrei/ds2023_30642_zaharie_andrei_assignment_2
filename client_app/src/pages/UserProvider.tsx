import axios from "axios";
import { createContext, useContext, useEffect, useMemo, useState } from "react";
import {CompatClient, Stomp} from "@stomp/stompjs";
import SockJS from "sockjs-client";
interface EnergyConsumptionMessage {
    userGuid: string;
    deviceGuid: string;
    currentConsumption: number;
    timeStamp: string;
}

interface UserContetType {
    token: string | null;
    setToken: (newToken: string | null) => void;
    isLogged: boolean | null;
    setIsLogged: (logged : boolean | null) => void;
    refreshPage: boolean | null;
    setRefreshPage: (refresh : boolean | null) => void;
    userGuid: string | null;
    setUserGuid: (guid : string | null) => void;
    energyConsumptionMessage: EnergyConsumptionMessage | null;
    setEnergyConsumptionMessage: (message: EnergyConsumptionMessage | null) => void;
    stompClient: CompatClient | null;
    setStompClient: (client: CompatClient | null) => void;
    chatClient: CompatClient | null;
    setChatClient: (client: CompatClient | null) => void;
}

export const UserContext = createContext<UserContetType>({
    token: null,
    setToken: () => {},
    isLogged: false,
    setIsLogged: () => {},
    refreshPage: true,
    setRefreshPage: () =>{},
    userGuid: null,
    setUserGuid: () => {},
    energyConsumptionMessage: null,
    setEnergyConsumptionMessage: () => {},
    stompClient: null,
    setStompClient: () => {},
    chatClient : null,
    setChatClient: () => {}
});

const UserProvider = ({ children }: { children: React.ReactNode }) => {
    const [token, setToken_] = useState<string | null>(
        localStorage.getItem("JwtToken")
    );
    const [isLogged, setIsLogged_] = useState<boolean | null>(false);
    const [refreshPage, setRefreshPage_] = useState<boolean | null>(true);
    const [userGuid, setUserGuid_] = useState<string | null>(localStorage.getItem("guid"));
    const [energyConsumptionMessage, setEnergyConsumptionMessage_] = useState<EnergyConsumptionMessage | null>(null);
    const [stompClient, setStompClient_] = useState<CompatClient | null>(null); // Added stompClient state
    const [chatClient, setChatClient_] = useState<CompatClient | null>(null); // Added stompClient state

    const setToken = (newToken: string | null) => {
        setToken_(newToken);
    };

    const setIsLogged = (logged: boolean | null) => {
        setIsLogged_(logged);
    };

    const setUserGuid = (guid: string | null) => {
        setUserGuid_(guid);
    };

    const setRefreshPage = (refresh: boolean | null) => {
        setRefreshPage_(refresh);
    };

    const setEnergyConsumptionMessage = (message: EnergyConsumptionMessage | null) => {
        setEnergyConsumptionMessage_(message);
    };

    const setStompClient = (client: CompatClient | null) => {
        setStompClient_(client);
    };

    const setChatClient = (client: CompatClient | null) => {
        setChatClient_(client);
    };

    useEffect(() => {
        if (token) {
            let jwtPayload = JSON.parse(atob(token.split('.')[1]));
            const email = jwtPayload.email;
            const role = jwtPayload.role;
            const guid = jwtPayload.guid
            localStorage.setItem("email", email);
            localStorage.setItem("role", role);
            localStorage.setItem("guid", guid);
            setIsLogged(true);
        } else {
            localStorage.clear();
        }
    }, [token]);

    const contextValue = useMemo(
        () => ({
            token,
            setToken,
            isLogged,
            setIsLogged,
            refreshPage,
            setRefreshPage,
            userGuid,
            setUserGuid,
            energyConsumptionMessage,
            setEnergyConsumptionMessage,
            stompClient,
            setStompClient,
            chatClient,
            setChatClient
        }),
        [userGuid,token, isLogged, refreshPage,energyConsumptionMessage,stompClient, chatClient]
    );

    return (
        <UserContext.Provider value={contextValue}>
            {children}
        </UserContext.Provider>
    );
};

export default UserProvider;