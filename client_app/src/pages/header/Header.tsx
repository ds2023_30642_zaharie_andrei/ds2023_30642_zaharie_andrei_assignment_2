import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import AdbIcon from '@mui/icons-material/Adb';
import {useNavigate} from "react-router-dom";
import "./Header.styles.css"
import {useEffect, useState, useContext} from "react";
import {UserContext} from "../UserProvider";
import SockJS from "sockjs-client"
import {Stomp} from "@stomp/stompjs";
const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];
export default function Header() {
    let navigate = useNavigate();
    const [anchorElUser, setAnchorElUser] = useState<null | HTMLElement>(null);
    const{setUserGuid,setToken, isLogged, setIsLogged, refreshPage} = useContext(UserContext);
    const [role, setRole] = useState(localStorage.getItem("role"));
    const isAdmin = () =>{
        if(role === null) {
            setRole("");
            return false;
        }
        return role === "admin";
    }

    useEffect(() => {

    }, [isLogged, role, refreshPage]);

    const logoutButton = async (e:any) => {
        setUserGuid(null);
        setToken(null);
        setIsLogged(false);
        navigate('/login');
    }
    const buttonClicked = (event: React.MouseEvent<HTMLElement>) =>{
        console.log(event.currentTarget.textContent);
        navigate("/" + event.currentTarget.textContent);
    }

    const settingsClicked = (event: React.MouseEvent<HTMLElement>) =>{
        console.log(event.currentTarget.textContent);
        if(event.currentTarget.textContent === 'Logout'){
            setIsLogged(false);
            setToken(null);
            navigate("/login");
        }else {
            navigate("/" + event.currentTarget.textContent);
        }
    }

    const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };

    return (
        <div className={"header"}>

            <AppBar position="static">
                <Container maxWidth="xl">
                    <Toolbar >
                        <div className={"catalogName"}>
                            <h4>
                                Device Manager
                            </h4>
                        </div>

                        <AdbIcon sx={{ display: { xs: 'flex', md: 'none' }, mr: 1 }} />

                        <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}>
                            {
                                isAdmin() && isLogged && (
                                        <Button
                                            value={"Devices"}
                                            onClick={buttonClicked}
                                            sx={{ my: 2, color: 'white', display: 'block' }}
                                        >
                                            Devices
                                        </Button>
                                )
                            }
                            {
                                isAdmin() && isLogged && (
                                        <Button
                                            value={"Users"}
                                            onClick={buttonClicked}
                                            sx={{ my: 2, color: 'white', display: 'block' }}
                                        >
                                            Users
                                        </Button>
                                )
                            }
                            {
                                isLogged ? (
                                    <Button
                                        value={"Logout"}
                                        onClick={logoutButton}
                                        sx={{ my: 2, color: 'white', display: 'block' }}
                                    >
                                        Log out
                                    </Button>
                                ):(
                                    <Button
                                        value={"Login"}
                                        onClick={logoutButton}
                                        sx={{ my: 2, color: 'white', display: 'block' }}
                                    >

                                    </Button>
                                )
                            }
                        </Box>
                        { isLogged?(
                        <Box sx={{ flexGrow: 0 }}>
                            <Tooltip title="Open settings">
                                <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                                    <Avatar alt="User" src="/static/images/avatar/2.jpg" />
                                </IconButton>
                            </Tooltip>
                            <Menu
                                sx={{ mt: '45px' }}
                                id="menu-appbar"
                                anchorEl={anchorElUser}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={Boolean(anchorElUser)}
                                onClose={handleCloseUserMenu}
                            >
                                {settings.map((setting) => (
                                    <MenuItem key={setting} onClick={handleCloseUserMenu}>
                                        <Typography textAlign="center" onClick={settingsClicked}>{setting}</Typography>
                                    </MenuItem>
                                ))}
                            </Menu>
                        </Box>):(<Box></Box>)
                        }
                    </Toolbar>
                </Container>
            </AppBar>
        </div>
    );
}

