import React, {useContext, useEffect, useState} from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { ThemeProvider } from '@emotion/react';
import CardsTheme from "./CardsTheme";
import ModalEditDevice from "../Modals/ModalEditDevice";
import ModalAcceptRemoveDevice from '../Modals/ModalAcceptRemoveDevice';
import UserProvider, {UserContext} from "../../UserProvider";
import ModalEnergyConsumption from "../Modals/ModalEnergyConsumption";
interface Device {
    name: string;
    description: string;
    address: string;
    energyConsumption: number;
    ownerGuid: string;
    guid: string;
    id: number;
}

interface DeviceConsumption {
    energyConsumption: number;
    timestamp: number;
}

interface DeviceCardProps {
    device: Device;
    userGuid:string | null;
}

const DeviceCard: React.FC<DeviceCardProps> = ({ device, userGuid}) => {
    const [openEdit, setOpenEdit] = React.useState(false);
    const [openRemove, setOpenRemove] = React.useState(false);
    const [selectedDevice, setSelectedDevice] = React.useState<Device | null>(null);
    const {token, setRefreshPage, energyConsumptionMessage} = useContext(UserContext);
    const [openEnergyModal, setOpenEnergyModal] = React.useState(false);
    const [energyConsumptions, setEnergyConsumptions] = useState<DeviceConsumption[]>([]);
    const [consumptionMap] = useState(new Map());

    const [energyAboveLimit, setEnergyAboveLimit] = useState(false);
    const handleEdit = (device: Device) => {
        setSelectedDevice(device);
        setOpenEdit(true);
    };

    const showCurrentConsumption = () => {
        if (energyConsumptionMessage === null)
            return 0;

        if (energyConsumptionMessage.deviceGuid === device.guid) {
            consumptionMap.set(device.guid, energyConsumptionMessage.currentConsumption);
            return energyConsumptionMessage.currentConsumption;
        }
        else{
            return consumptionMap.get(device.guid);
        }
    }

    useEffect(() => {
        if(device.ownerGuid === energyConsumptionMessage?.userGuid &&
           device.guid === energyConsumptionMessage?.deviceGuid)
        {
            if(device.energyConsumption < energyConsumptionMessage.currentConsumption){
                setEnergyAboveLimit(true);
            }
            else{
                setEnergyAboveLimit(false);
            }
        }
    }, [energyConsumptionMessage]);

    const openRemoveModal = (device: Device) => {
        setSelectedDevice(device);
        setOpenRemove(true);
    };
    const handleRemove = async (deviceId: number) =>{
        if(deviceId){
            const result = await fetch(`http://localhost:8081/api/devices/removeOwner/${deviceId}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`
                }
            });
            let data = await result.json();
            if (result.ok) {
                console.log(data);
                setRefreshPage(true);
            } else {
                console.log("ModalEditDevice - error could not remove owner for device!");
            }
        }
        handleModalClose();
    }

    const handleEnergyConsumptions = async (device: Device) => {
        try {
            const response = await fetch(`http://localhost:8082/api/energyConsumption/${device.guid}`);
            if (response.ok) {
                const data: DeviceConsumption[] = await response.json();
                setEnergyConsumptions(data);
                setOpenEnergyModal(true);
            } else {
                console.error('Failed to fetch energy consumptions');
            }
        } catch (error) {
            console.error('Error fetching energy consumptions:', error);
        }
    };

    const handleDateChange = async (selectedDate: Date) => {
        try {
            const response = await fetch(`http://localhost:8082/api/energyConsumption/${device.guid}`);
            if (response.ok) {
                const data: DeviceConsumption[] = await response.json();
                const filteredData = data.filter((consumption) => {
                    const consumptionDate = new Date(consumption.timestamp);
                    return (
                        consumptionDate.getDate() === selectedDate.getDate() &&
                        consumptionDate.getMonth() === selectedDate.getMonth() &&
                        consumptionDate.getFullYear() === selectedDate.getFullYear()
                    );
                });
                setEnergyConsumptions(filteredData);
            } else {
                console.error('Failed to fetch energy consumptions');
            }
        } catch (error) {
            console.error('Error fetching energy consumptions:', error);
        }
    };


    const handleModalClose = () => {
        setSelectedDevice(null);
        setOpenEdit(false);
        setOpenRemove(false);
        setOpenEnergyModal(false);
    };


    return (
        <ThemeProvider theme={CardsTheme}>
        <Card sx={{ maxWidth: 345 }}>
            <CardMedia
                title={device.name}>
                <img
                    src="/devices.png"
                    alt="logo"
                    width="120"
                    height="120"
                />
            </CardMedia>
            { energyAboveLimit &&
            <CardMedia>
                <img
                    src="/warning.png"
                    alt="logo"
                    width="30"
                    height="30"
                />
            </CardMedia>
            }
            <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                    {device.name}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    Description: {device.description}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    Address: {device.address}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    Max Energy Consumption: {device.energyConsumption} kWh
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    Current Total Consumption: { showCurrentConsumption() } kWh
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small" onClick={() => handleEdit(device)}>Edit</Button>
                <Button size="small" color={"error"} onClick={() => openRemoveModal(device)}>Remove</Button>
                <Button size="small" onClick={() => handleEnergyConsumptions(device)}>Chart</Button>
            </CardActions>
        </Card>
            { selectedDevice &&
                (
                    <div>
                        <ModalEditDevice open={openEdit} onClose={handleModalClose} device={selectedDevice} />
                        <ModalAcceptRemoveDevice open={openRemove} onClose={handleModalClose} onRemove={() =>handleRemove(selectedDevice?.id)} device={selectedDevice} />
                    </div>
                )
            }
            <div>
                <ModalEnergyConsumption open={openEnergyModal} onClose={handleModalClose} energyConsumptions={energyConsumptions} deviceName={device.name} maxConsumption={device.energyConsumption} onDateChange={handleDateChange}/>
            </div>
        </ThemeProvider>

    );
}

export default DeviceCard;
