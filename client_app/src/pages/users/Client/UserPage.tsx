import React, {useState, useEffect, useContext} from 'react';
import DeviceCards from './DeviceCards';
import {UserContext} from "../../UserProvider";
import {ChatMessage, Device, User} from "../User.types";
import Grid from '@mui/material/Grid';
import SockJS from "sockjs-client";
import {Stomp} from "@stomp/stompjs";
import IconButton from "@mui/material/IconButton";
import ChatModal from "../../chat/ChatModal";

function UserPage() {

    const [devices, setDevices] = useState<Device[]>([]);
    const {
        refreshPage,
        setRefreshPage,
        userGuid,
        setUserGuid,
        isLogged,
        chatClient,
        setChatClient,
    } = useContext(UserContext);
    const role = localStorage.getItem("role");
    const email = localStorage.getItem("email");
    const [userChatMessage, setUserChatMessage] = useState<ChatMessage>();
    const [isModalOpened, setIsModalOpened] = useState<boolean>(false);
    const [selectedUser, setSelectedUser] = useState<User | null>(null);
    const [incomingMessage, setIncomingMessage] = useState<ChatMessage | null>(null);
    const {token} = useContext(UserContext);
    function openChatModal(){
        setIsModalOpened(true);
        setSelectedUser({
            guid: userGuid != null ? userGuid : "",
            email: "staff@yahoo.com",
            name: "Admin",
            role: role !=null ? role: "",
            deviceList: devices
        })
    }

    function handleModalClose(){
        setIsModalOpened(false);
        setSelectedUser(null);
    }

    function disconnectWebSocket(client: any){
        const connectMessage = "User has disconnected"; // Message to send to the backend
        client.send(`/app/disconnectChat/${userGuid}`, {}, connectMessage); //trebuie sa implementez asta !
    }

    useEffect(() => {
        if(!userGuid){
            const guid = localStorage.getItem("guid");
            if(guid){
                setUserGuid(guid);
            }
        }
        async function fetchDevices() {
            try {
                const response = await fetch(`http://localhost:8081/api/devices/getUserDevices/${userGuid}`, {
                    method: 'GET',
                    headers: {
                        Authorization: `Bearer ${token}`,
                    }
                });

                if (response.ok) {
                    const data = await response.json();
                    setDevices(data);
                } else {
                    console.error('Failed to fetch devices:', response.status);
                }
            } catch (error) {
                console.error('Error fetching devices:', error);
            }
        }
        if(userGuid) {
            fetchDevices()
                .then(() => setRefreshPage(false));
        }
    }, [refreshPage, userGuid]);

    useEffect(() => {
         if(chatClient == null && role === "user") {
               const userWebSocket = new SockJS('http://localhost:8083/chat_socket')
               const userClient = Stomp.over(userWebSocket);
               userWebSocket.onopen = () => {
                   console.log("[User]: Websocket for chat opened!");
               }
               userClient.connect({}, () => {
                   userClient.subscribe(`/chat/${userGuid}/message`, (message) => {
                       console.log('[User]Received message:', message.body);
                       var jsonMesssage = JSON.parse(message.body);
                       console.log("Message json is ", jsonMesssage);
                       setIncomingMessage(jsonMesssage);
                   });

               });
               setChatClient(userClient);
               return () => {
                   if (userClient && userClient.connected) {
                       disconnectWebSocket(userClient);
                       userClient.disconnect(() => {
                           console.log('[User] :Chat WebSocket connection closed');
                       });
                       setChatClient(null);
                   }
               };
        }
    }, [userGuid, isLogged]);

    return (
        <div>
            <h1>User Page</h1>
            <div className={"user-cards"}>
            <Grid container spacing={2}>
            {devices.map((device:Device, index) => (
                <Grid item xs={4}>
                <DeviceCards
                    key={index}
                    device={device}
                    userGuid={userGuid}
                />
                </Grid>
            ))}
            </Grid>
            </div>
            <div className="chatIcon">
                <IconButton onClick={openChatModal}>
                    <img className="iconImage" src="/liveChat.png" alt="Live Chat Icon" />
                </IconButton>
            </div>
            <ChatModal open={isModalOpened} onClose={handleModalClose} user={selectedUser} recievedMessage={incomingMessage}/>
        </div>
    );
}

export default UserPage;
