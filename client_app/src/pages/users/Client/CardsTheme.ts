import { createTheme } from '@mui/material/styles';

const CardsTheme = createTheme({
    components: {
        MuiCard: {
            styleOverrides: {
                root: {
                    boxShadow: '0 10px 20px rgba(0, 0, 0, 0.2)', // Adjust the shadow as needed
                },
            },
        },
    },
});

export default CardsTheme;