import React, {useState, useEffect, useContext} from 'react';
import {UserContext} from "../../UserProvider";
import {Device, User} from "../User.types"
import "../User.styles.css"
import Button from "@mui/material/Button";
import ModalEditDevice from "../Modals/ModalEditDevice";
import ModalEditUser from "../Modals/ModalEditUser";
import DeviceTable from "./DeviceTable";
import UserTable from "./UserTable";
import ChatModal from "../../chat/ChatModal";

function AdminPage() {
    const [users, setUsers] = useState<User[]>([]);
    const [devices, setDevices] = useState<Device[]>([]);
    const {token, refreshPage, setRefreshPage} = useContext(UserContext);
    const [isDeviceModalOpen, setIsDeviceModalOpen] = useState(false);
    const [isUserModalOpen, setIsModelUserOpen] = useState(false);

    const addNewDeviceModal = () => {
        setIsDeviceModalOpen(true);
    };

    const addNewUserModal = () => {
        setIsModelUserOpen(true);
    };
    useEffect(() => {
        async function fetchUsers() {
            try {
                const response = await fetch('http://localhost:8080/api/users/all', {
                    method: 'GET',
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });

                if (response.ok) {
                    const data = await response.json();
                    setUsers(data);
                } else {
                    console.error('Failed to fetch users:', response.status);
                }
            } catch (error) {
                console.error('Error fetching users:', error);
            }
        }

        async function fetchDevices() {
            try {
                const response = await fetch('http://localhost:8081/api/devices/all', {
                    method: 'GET',
                    headers: {
                        Authorization: `Bearer ${token}`,
                    }
                });

                if (response.ok) {
                    const data = await response.json();
                    setDevices(data);
                } else {
                    console.error('Failed to fetch devices:', response.status);
                }
            } catch (error) {
                console.error('Error fetching devices:', error);
            }
        }

        fetchUsers()
            .then(() => fetchDevices())
            .then(() => setRefreshPage(false));
    }, [refreshPage]);


    return (
        <div className="main-page">
            <h1 className="admin-title">Admin Page</h1>
            <div className="admin-container">
                <div className="grid-container">
                    <div className="user-table left-table">
                        <h4>Users Table</h4>
                        <UserTable users={users} devices={devices}/>
                        <div className={"add-button"}>
                            <Button variant="contained" color="primary" onClick={addNewUserModal}>
                                Add New User
                            </Button>
                        </div>
                    </div>
                    <div className="device-table right-table">
                        <h4>Devices Table</h4>
                        <DeviceTable devices={devices}/>
                        <div className={"add-button"}>
                            <Button variant="contained" color="primary" onClick={addNewDeviceModal}>
                                Add New Device
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
            <ModalEditDevice open={isDeviceModalOpen} onClose={() => setIsDeviceModalOpen(false)} device={null}/>
            <ModalEditUser open={isUserModalOpen} onClose={() => setIsModelUserOpen(false)} user={null} />
        </div>
    );
}

export default AdminPage;
