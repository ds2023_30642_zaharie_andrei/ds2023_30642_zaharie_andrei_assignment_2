import * as React from 'react';
import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import {ChatMessage, Device, User} from "../User.types"
import Button from "@mui/material/Button";
import ModalEditDevice from "../Modals/ModalEditDevice";
import ModalEditUsers from "../Modals/ModalEditUser";
import {useContext, useEffect, useState} from "react";
import ModalLinkDevice from "../Modals/ModalLinkDevice";
import {UserContext} from "../../UserProvider";
import ChatModal from "../../chat/ChatModal";
import SockJS from "sockjs-client";
import {Stomp} from "@stomp/stompjs";
import {useNavigate} from "react-router-dom";


interface UserTableProps {
    users: User[];
    devices: Device[];
}

function mapDevicesToUser(user: User, devices: Device[]): User {
    const userDevices = devices.filter(device => device.ownerGuid === user.guid);
    user.deviceList = userDevices;

    return user;
}

function Row(props: { user: User, devices: Device[], onEdit: (user: User) => void, onLinkDevice:(user : User) => void, onChatOpen:(user: User) => void}) {
    const {user, devices,onEdit, onLinkDevice, onChatOpen} = props;
    const [open, setOpen] = React.useState(false);
    const row = mapDevicesToUser(user, devices);
    const {token, setRefreshPage} = useContext(UserContext);
    const handleEditClick = () => {
        onEdit(user);
    };

    const handleRemoveDevice = async (deviceId : number) =>{
        if (deviceId) {
            const result = await fetch(`http://localhost:8081/api/devices/removeOwner/${deviceId}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`
                }
            });
            let data = await result.json();
            if(result.ok){
                console.log(data);
                setRefreshPage(true);
            }
            else{
                console.log("ModalEditDevice - error could not remove device from user!");
            }
        }
    }

    const handleLinkDevice =() =>{
        onLinkDevice(user);
    }
    const handleOpenChat =() =>{
        onChatOpen(user);
    }
    return (
        <React.Fragment>
            <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
                <TableCell>
                    <IconButton
                        aria-label="expand row"
                        size="small"
                        onClick={() => setOpen(!open)}
                    >
                        {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                <TableCell component="th" scope="row">
                    {row.name}
                </TableCell>
                <TableCell align="center">{row.email}</TableCell>
                <TableCell align="center">{row.role}</TableCell>
                <TableCell>
                    <Button
                        variant="outlined"
                        size="small"
                        onClick={handleEditClick}
                    >
                        Edit
                    </Button>
                    {row.role === "user" &&
                    <Button
                        variant="outlined"
                        size="small"
                        onClick={handleOpenChat}
                    >
                        Chat
                    </Button>
                    }
                </TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <Box sx={{ margin: 1 }}>
                            <Typography variant="h6" gutterBottom component="div">
                                Devices
                            </Typography>
                            <Table size="small" aria-label="purchases">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Nr.</TableCell>
                                        <TableCell>Name</TableCell>
                                        <TableCell>Address</TableCell>
                                        <TableCell align="right">Description</TableCell>
                                        <TableCell align="right">Energy Consumption(kWh)</TableCell>
                                        <TableCell>
                                            <Button
                                                variant="contained"
                                                size="small"
                                                color="primary"
                                                onClick={handleLinkDevice}
                                            >
                                                Link Device
                                            </Button>
                                        </TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {row.deviceList?.map((device, index) => (
                                        <TableRow key={index}>
                                            <TableCell>{index +1}</TableCell>
                                            <TableCell>{device.name}</TableCell>
                                            <TableCell>{device.address}</TableCell>
                                            <TableCell align="center">{device.description}</TableCell>
                                            <TableCell align="center">{device.energyConsumption} kWh</TableCell>
                                            <TableCell>
                                                <Button
                                                    variant="outlined"
                                                    size="small"
                                                    color="error"
                                                    onClick={() => handleRemoveDevice(device.id)}
                                                >
                                                    Remove
                                                </Button>
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
}

const UserTable: React.FC<UserTableProps> = ({ users, devices }) => {
    const [selectedUser, setSelectedUser] = useState<User | null>(null);
    const [selectedDevice, setSelecteDevice] = useState<Device | null>(null);
    const [editUser, setEditUser] = useState(false);
    const [linkDevices, setLinkDevices] = useState(false);
    const [isChatModalOpen, setIsChatModalOpen] = useState(false);
    const role = localStorage.getItem("role");
    const {chatClient, setChatClient, userGuid, isLogged} = useContext(UserContext);
    const [incomingMessage, setIncomingMessage] = useState<ChatMessage | null>(null);

    useEffect(() => {
        if(role === 'admin') {
            if (chatClient == null) {
                const adminWebSocket = new SockJS('http://localhost:8083/chat_socket')
                const adminClient = Stomp.over(adminWebSocket);
                adminWebSocket.onopen = () => {
                    console.log("[Admin]: Websocket for chat opened!");
                }
                adminClient.connect({}, () => {
                    adminClient.subscribe(`/chat/message/admin`, (message) => {
                        console.log('[Admin]Received message:', message.body);
                        var jsonMesssage = JSON.parse(message.body);

                        setIncomingMessage(jsonMesssage);
                    });
                    //sendTest(userClient);
                });
                setChatClient(adminClient);
                return () => {
                    if (adminClient && adminClient.connected) {
                        //disconnectWebSocket(userClient);
                        adminClient.disconnect(() => {
                            console.log('[Admin] :Chat WebSocket connection closed');
                        });
                        setChatClient(null);
                    }
                };
            }
        }
    }, [role,isLogged]);


    const openChatModal = (user :User) => {
        setSelectedUser(user);
        setIsChatModalOpen(true);
    };
    const handleEdit = (user: User) => {
        setSelectedUser(user);
        setEditUser(true);
    };

    const handleModalClose = () => {
        setSelectedUser(null);
        setEditUser(false);
        setLinkDevices(false);
        setIsChatModalOpen(false);
    };

    const handleLinkDevice = (user: User) =>{
        setSelectedUser(user);
        setLinkDevices(true);
    }


    return (
        <TableContainer component={Paper}>
            <Table stickyHeader aria-label="sticky table">
                <TableHead>
                    <TableRow>
                        <TableCell />
                        <TableCell>Name</TableCell>
                        <TableCell align="center">Email</TableCell>
                        <TableCell align="center">Role&nbsp;</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {users.map((user) => (
                        <Row key={user.guid} user={user} devices={devices} onEdit={handleEdit} onLinkDevice={handleLinkDevice} onChatOpen={openChatModal}/>
                    ))}
                </TableBody>
            </Table>
            <ModalEditUsers open={editUser} onClose={handleModalClose} user={selectedUser} />
            <ModalLinkDevice open={linkDevices} onClose={handleModalClose} user={selectedUser} devices={devices}/>
            <ChatModal open={isChatModalOpen} onClose={handleModalClose} user={selectedUser} recievedMessage={incomingMessage}/>
        </TableContainer>
    );
}

export default UserTable;