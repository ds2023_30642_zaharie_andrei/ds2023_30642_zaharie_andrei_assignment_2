export type Device ={
    name: string;
    description: string;
    address: string;
    energyConsumption: number;
    ownerGuid : string;
    guid: string;
    id : number;
}

export type User = {
    guid: string
    email: string;
    name: string;
    role: string;
    deviceList: Device[];
}

export type UserRequest = {
    guid: string
    email: string;
    name: string;
    role: string;
}

export type DeviceConsumption = {
    timestamp: number;
    energyConsumption: number;
}

export type ChatMessage = {
    sendFrom: string;
    sendTo: string;
    message: string;
    type: string;
    timestamp: number;
}