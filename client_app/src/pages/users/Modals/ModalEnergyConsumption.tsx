import React from 'react';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import Button from '@mui/material/Button';
import { DeviceConsumption } from "../User.types";
import '../User.styles.css';
import EnergyChart from "../../utils/EnergyChart";
import Calendar from "../../utils/Calendar";


interface EnergyConsumptionModalProps {
    open: boolean;
    onClose: () => void;
    energyConsumptions: DeviceConsumption[];
    deviceName: string;
    maxConsumption: number;
    onDateChange: (date: Date) => void; // Function to handle date change
}

const ModalEnergyConsumption: React.FC<EnergyConsumptionModalProps> = ({ open, onClose, energyConsumptions, deviceName, maxConsumption, onDateChange }) => {
    return (
        <div className={"remove-modal"}>
            <Dialog open={open} onClose={onClose}>
                <DialogTitle>Consumption Chart</DialogTitle>
                <DialogContent>
                    <h3>{deviceName}</h3>
                    {energyConsumptions.length > 0 ?
                    <EnergyChart deviceConsumptions={energyConsumptions} maxConsumption={maxConsumption} />
                        :
                        <p>This device does not have a consumption history yet! Make sure it is working and plugged in.</p>
                    }
                    <h4>Choose the day</h4>
                    <Calendar onChange={onDateChange} />
                </DialogContent>
                <DialogActions>
                    <Button onClick={onClose} color="primary">
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
};

export default ModalEnergyConsumption;
