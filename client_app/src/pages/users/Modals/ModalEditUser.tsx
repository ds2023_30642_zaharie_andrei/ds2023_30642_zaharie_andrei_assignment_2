import React, { useContext, useEffect, useState } from 'react';
import Modal from "@mui/material/Modal";
import Button from "@mui/material/Button";
import {User, UserRequest} from "../User.types";
import { TextField } from "@mui/material";
import '../User.styles.css';
import { UserContext } from "../../UserProvider";
import {IUserRegisterRequest} from "../../authentication/Authentication.types";

interface ModalEditUserProps {
    open: boolean;
    onClose: () => void;
    user: User | null;
}

function mapUserToUserRequest(user: User): UserRequest {
    const userRequest: UserRequest = {
        guid: user.guid,
        email: user.email,
        name: user.name,
        role: user.role,
    };
    return userRequest;
}

function mapUserToUserRegisterRequest(user: User, password:string): IUserRegisterRequest {
    const userRegisterRequest: IUserRegisterRequest = {
        email: user.email,
        name: user.name,
        role: user.role,
        password: password
    };
    return userRegisterRequest;
}

const ModalEditUser: React.FC<ModalEditUserProps> = ({ open, onClose, user }) => {
    const [editedUser, setEditedUser] = useState<User | null>();
    const { setRefreshPage, token } = useContext(UserContext);
    const [editingUser, setEditingUser] = useState(false);
    const [userAddPassword, setUserAddPassword] = useState("");

    useEffect(() => {
        if (user) {
            console.log("editing user");
            console.log(editingUser);
            setEditingUser(true);
            setEditedUser({ ...user });
        } else {
            setEditingUser(false);
            setEditedUser({
                guid: "",
                name: "",
                email: "",
                role: "",
                deviceList: [],
            });
        }
    }, [user]);

    const handleEdit = async () => {
        console.log(editedUser);
        if (editedUser) {
            const userRequest: UserRequest = mapUserToUserRequest(editedUser);
            const result = await fetch(`http://localhost:8080/api/users/updateUser/${userRequest.guid}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`
                },
                body: JSON.stringify(userRequest),

            });
            let data = await result.json();
            if (result.ok) {
                console.log(data);
                setRefreshPage(true);
            } else {
                console.log("ModalEditUser - error could not edit user!");
            }
        }
        onClose();
    };

    const handleDelete = async () => {
        if (editedUser) {
            const result = await fetch(`http://localhost:8080/api/users/deleteUser/${editedUser.guid}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`
                }
            });
            if (result.ok) {
                let data = await result.json();
                console.log(data);
                setRefreshPage(true);
            } else {
                console.log("ModalEditUser - error could not delete user!");
            }
            const resultRemoveOwner = await fetch(`http://localhost:8081/api/devices/removeOwner/${editedUser.guid}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`
                }
            });
            if (resultRemoveOwner.ok) {
                let data = await resultRemoveOwner.json();
                console.log(data);
                setRefreshPage(true);
            } else {
                console.log("ModalEditUser - error could not delete user!");
            }
        }
        onClose();
    };

    const handleAdd = async () => {
        if (editedUser) {
            const userRegisterRequest: IUserRegisterRequest = mapUserToUserRegisterRequest(editedUser, userAddPassword);
            const result = await fetch('http://localhost:8080/api/users/addUser', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`
                },
                body: JSON.stringify(userRegisterRequest)
            });
            let data = await result.json();
            if (result.ok) {
                console.log(data);
                setRefreshPage(true);
            } else {
                console.log("ModalEditUser - error could not add a new user!");
            }
        }
        onClose();
    };

    return (
        <Modal open={open} onClose={onClose}>
            <div className={"modal-container"}>
                <form onSubmit={handleEdit}>
                    {editedUser && (
                        <div>
                            {editingUser ? (<h2>Edit User</h2>) : (<h2>Add new User</h2>)}
                            <div className="form-container">
                                <TextField
                                    className="text-field"
                                    label="Name"
                                    value={editedUser.name}
                                    onChange={(e) => setEditedUser({ ...editedUser, name: e.target.value })}
                                />
                                <TextField
                                    className="text-field"
                                    label="Email"
                                    value={editedUser.email}
                                    onChange={(e) => setEditedUser({ ...editedUser, email: e.target.value })}
                                />
                                {!editingUser &&(
                                    <TextField
                                        className="text-field"
                                        label="Password"
                                        type="password"
                                        value={userAddPassword}
                                        onChange={(e) => setUserAddPassword(e.target.value)}
                                    />
                                )
                                }
                                <TextField
                                    className="text-field"
                                    label="Role"
                                    value={editedUser.role}
                                    onChange={(e) => setEditedUser({ ...editedUser, role: e.target.value })}
                                />
                                {editingUser ? (
                                    <div className="buttons-container">
                                        <Button variant="contained" color="primary" onClick={handleEdit}>
                                            Edit
                                        </Button>
                                        <Button variant="outlined" color="error" onClick={handleDelete}>
                                            Delete
                                        </Button>
                                    </div>
                                ) : (
                                    <div className="buttons-container">
                                        <Button variant="contained" color="primary" onClick={handleAdd}>
                                            Add
                                        </Button>
                                    </div>
                                )}
                            </div>
                        </div>
                    )}
                </form>
            </div>
        </Modal>
    );
}

export default ModalEditUser;
