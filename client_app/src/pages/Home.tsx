import {useContext, useEffect, useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import axios from "axios";
import AdminPage from "./users/Admin/AdminPage";
import {UserContext} from "./UserProvider";
import UserPage from "./users/Client/UserPage";
import SockJS from "sockjs-client"
import {Stomp} from "@stomp/stompjs";

export default function Home() {
    const {chatClient, setChatClient, setStompClient, isLogged ,token, setToken, energyConsumptionMessage, setEnergyConsumptionMessage} = useContext(UserContext);
    let navigate = useNavigate();
    const email = localStorage.getItem("email");
    const role = localStorage.getItem("role");
    const [isAdmin, setIsAdmin] = useState(false);
    const userApi= axios.create({
        baseURL: "http://localhost:8080/api/"
    });
    const {userGuid} = useContext(UserContext);
    function sendMessage(client: any){
        const connectMessage = "User has connected"; // Message to send to the backend
        client.send(`/app/connect/${userGuid}`, {}, connectMessage);
    }

    function sendTest(userClient:any){
        const message = "Teste coaieee";
        userClient.send(`/app/chat/message/${userGuid}`,{},message);
    }
    function disconnectWebSocket(client: any){
        const connectMessage = "User has disconnected"; // Message to send to the backend
        client.send(`/app/disconnect/${userGuid}`, {}, connectMessage);
    }

    useEffect(() => {
        //check if user still exists and delete the token if not
        checkUser()
    }, [token]);

    useEffect(() => {
        if(role === 'user'){
            setIsAdmin(false);
           /* if(chatClient == null) {
                const userWebSocket = new SockJS('http://localhost:8083/chat_socket')
                const userClient = Stomp.over(userWebSocket);
                userWebSocket.onopen = () => {
                    console.log("[User]: Websocket for chat opened!");
                }
                userClient.connect({}, () => {
                    userClient.subscribe(`/chat/${userGuid}/message`, (message) => {
                        console.log('Received message:', message.body);
                        var jsonMesssage = JSON.parse(message.body);
                        console.log("Message json is ", jsonMesssage)
                    });
                    sendTest(userClient);
                });
                setChatClient(userClient);
                return () => {
                    if (userClient && userClient.connected) {
                        disconnectWebSocket(userClient);
                        userClient.disconnect(() => {
                            console.log('[User] :Chat WebSocket connection closed');
                        });
                        setChatClient(null);
                    }
                };
            }*/
        }else if(role === 'admin'){
            setIsAdmin(true);
        }
    }, [token, role]);

    useEffect(() => {
        const websocket = new SockJS('http://localhost:8082/socket');
        const client = Stomp.over(websocket);
        websocket.onopen = () => {
            console.log('WebSocket connection established');
        };

        client.connect({}, () => {
            console.log('Stomp client connected');
            // Subscription and message sending here
            client.subscribe(`/user/${userGuid}/message`, (message) => {
                console.log('Received message:', message.body);
                setEnergyConsumptionMessage(JSON.parse(message.body));
            });
            sendMessage(client);
        });
        setStompClient(client);

        client.onStompError = (frame) => {
            console.error('STOMP error:', frame);
        };
        client.onWebSocketError = (event) => {
            console.error('WebSocket error:', event);
        };

        return () => {
            if (client && client.connected) {
                disconnectWebSocket(client);
                client.disconnect(() => {
                    console.log('WebSocket connection closed');
                });
                setStompClient(null);
            }
        };
    }, [userGuid, isLogged]);
    async function checkUser() {
        axios.defaults.headers.common["Authorization"] = `Bearer ${token}`
       // setLoading(true);
        const result = await fetch(`http://localhost:8080/api/users/check/${userGuid}`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
        if(result.ok){
            const data = await result.json();
            console.log("data for refresh", data);
            if(!data){
                setToken(null);
                navigate("/login");
            }
        }
        else{
            if(result.status === 403){
                setToken(null);
                navigate("/login");
            }
           // setError('Invalid login!');
        }
    }

    return (
        <div>
            {isAdmin?(
                <AdminPage />
            ):(
                <UserPage />
            )
            }

        </div>
    );
}
