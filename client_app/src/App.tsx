import React, {useContext, useState} from 'react';
import logo from './logo.svg';
import './App.css';
import {Navigate, Route, Routes, useNavigate} from "react-router-dom";
import UserProvider, {UserContext} from "./pages/UserProvider";
import Header from "./pages/header/Header";
import PrivateRoute from "./pages/PrivateRoute";
import Home from "./pages/Home";
import Register from "./pages/authentication/Register";
import Login from "./pages/authentication/Login";

function App() {
  const [value, setValue] = useState(false);
  const {token, setToken} = useContext(UserContext);
  let navigate = useNavigate();
  return (
      <div className="App">
        <UserProvider>
          <Header />
          <Routes>
            <Route path="/" element={<PrivateRoute />}>
              <Route index element={<Home />} />
            </Route>
            <Route path="/register" element={<Register />} />
            <Route path="/login" element ={<Login />} />

          </Routes>
        </UserProvider>
      </div>

  );
}
export default App;
