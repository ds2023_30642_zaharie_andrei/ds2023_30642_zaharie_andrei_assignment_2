package com.example.sensorproducer.Controllers;

import com.example.sensorproducer.Models.DeviceEnergyConsumption;
import com.example.sensorproducer.Producer.RabbitMqProducer;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/test")
@RequiredArgsConstructor
public class RabbitController {

    private final RabbitMqProducer producer;

    @PostMapping("hello")
    public ResponseEntity<?> hello(@RequestBody DeviceEnergyConsumption device){
        producer.send(device);
        return ResponseEntity.ok(device);
    }
}

