package com.example.sensorproducer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SensorproducerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SensorproducerApplication.class, args);
    }

}
