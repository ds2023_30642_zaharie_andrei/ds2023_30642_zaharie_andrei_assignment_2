package com.example.deviceservice.Repository.Models;

import jakarta.persistence.*;
import lombok.Getter;

import java.util.UUID;

@Getter
@Entity
@Table(name = "device")
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String guid;
    private String name;
    private String description;
    private String address;
    private float energyConsumption;
    private String ownerGuid;

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setEnergyConsumption(float energyConsumption) {
        this.energyConsumption = energyConsumption;
    }

    public void setOwnerGuid(String ownerId) {
        this.ownerGuid = ownerId;
    }

    public Device(){}

    public Device(String name, String description, String address, float energyConsumption, String ownerId) {
        this.name = name;
        this.description = description;
        this.address = address;
        this.energyConsumption = energyConsumption;
        this.ownerGuid = ownerId;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @PrePersist
    public void prePersist() {
        if (this.guid == null) {
            this.guid = UUID.randomUUID().toString();
        }
    }
}
