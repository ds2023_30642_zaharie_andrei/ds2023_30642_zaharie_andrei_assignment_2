package com.example.deviceservice.Repository;

import com.example.deviceservice.Repository.Models.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface IDeviceRepository extends JpaRepository<Device, Long> {
    @Modifying
    @Query("UPDATE Device d SET d.ownerGuid = null WHERE d.ownerGuid = ?1")
    void deleteUserDevices(String userGuid);
    @Query("SELECT d FROM Device d WHERE d.guid = ?1")
    Optional<Device> findDeviceByGuid(String guid);
    @Query("SELECT d FROM Device d WHERE d.ownerGuid = ?1")
    Optional<List<Device>> findAllUserDevices(String ownerId);
}
