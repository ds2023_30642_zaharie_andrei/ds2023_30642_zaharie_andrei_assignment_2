package com.example.deviceservice.Producers;

import com.example.deviceservice.Producers.Models.SyncDevice;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


@Service
public class RabbitMQProducer {
    @Value("${rabbitmq.exchange.name}")
    private String exchange;
    @Value("${rabbitmq.routing.key}")
    private String routingKey;
    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMQProducer.class);
    private final RabbitTemplate producerTemplate;
    @Autowired
    public RabbitMQProducer(RabbitTemplate producerTemplate) {
        this.producerTemplate = producerTemplate;
    }

    public void send(SyncDevice syncDevice){
        producerTemplate.convertAndSend(exchange, routingKey, syncDevice);
        LOGGER.info("Device sent for db synchronization -> \n"+ syncDevice.toString());
    }

    private Message convertToJsonMessage(SyncDevice device){
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());

        try {
            String jsonPayload = objectMapper.writeValueAsString(device);
            MessageProperties properties = new MessageProperties();
            properties.setContentType("application/json");

            return new Message(jsonPayload.getBytes(), properties);

        } catch (JsonProcessingException e) {
            LOGGER.error("Could not convert the object to json");
            throw new RuntimeException(e);
        }
    }
}

