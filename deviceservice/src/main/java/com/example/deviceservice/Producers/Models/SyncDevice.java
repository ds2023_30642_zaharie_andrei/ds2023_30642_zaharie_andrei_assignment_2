package com.example.deviceservice.Producers.Models;

import lombok.Data;

@Data
public class SyncDevice {
    private String deviceGuid;
    private String ownerGuid;
    private float maxEnergyConsumption;
    private String syncType;

    public SyncDevice(){}

    public SyncDevice(String deviceGuid, String ownerGuid, float maxEnergyConsumption, String syncType){
        this.deviceGuid = deviceGuid;
        this.ownerGuid = ownerGuid;
        this.maxEnergyConsumption = maxEnergyConsumption;
        this.syncType = syncType;
    }
    @Override
    public String toString(){
        return "DeviceGuid: " + deviceGuid + "\nOwnerGuid: " + ownerGuid + "\n MaxEnergy: " + maxEnergyConsumption;
    }
}
