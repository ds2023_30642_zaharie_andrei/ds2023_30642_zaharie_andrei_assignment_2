package com.example.deviceservice.Controllers.Models;

import lombok.Data;

@Data
public class LinkDeviceRequest {
    private String ownerGuid;
    private long deviceId;
}
