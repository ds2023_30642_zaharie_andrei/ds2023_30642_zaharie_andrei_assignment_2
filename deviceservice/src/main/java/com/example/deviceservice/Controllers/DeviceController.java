package com.example.deviceservice.Controllers;

import com.example.deviceservice.Controllers.Models.LinkDeviceRequest;
import com.example.deviceservice.Repository.Models.Device;
import com.example.deviceservice.Services.DeviceService;
import com.example.deviceservice.Services.SynchronizationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/devices")
@CrossOrigin()
public class DeviceController {
    private final DeviceService deviceService;
    private final SynchronizationService syncService;


    @GetMapping("/getDevice")
    public ResponseEntity<?> getDevice(@RequestAttribute int id){
        Device device = this.deviceService.getDevice(id);
        return ResponseEntity.ok(device);
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAllDevices(){
        List<Device> devices = this.deviceService.getAllDevice();

        return ResponseEntity.ok(devices);
    }

    @PostMapping("/addDevice")
    public ResponseEntity<?> addDevice(@RequestBody Device newDevice){
        Device device = deviceService.save(newDevice);
        //syncronize the monitoring db
        syncService.syncMonitoringDatabase(device, "add");
        return ResponseEntity.ok(device);
    }

    @DeleteMapping("/deleteDevice/{deviceId}")
    public ResponseEntity<?> deleteDevice(@PathVariable long deviceId){
        Device deleteDevice = deviceService.getDevice(deviceId);
        deviceService.deleteDevice(deviceId);

        syncService.syncMonitoringDatabase(deleteDevice,"delete"); //sync for delete device

        return ResponseEntity.ok(HttpEntity.EMPTY);
    }

    @PostMapping("/deleteUserDevices/{userGuid}")
    public ResponseEntity<?> deleteUserDevices(@PathVariable String userGuid){
        List<Device> devices = deviceService.deleteUserDevices(userGuid);

        syncService.syncMonitoringDatabase(devices, "removeOwner");

        return ResponseEntity.ok(HttpEntity.EMPTY);
    }

    @PostMapping("/updateDevice/{deviceId}")
    public ResponseEntity<Device> updateDevice(@RequestBody Device updateDevice, @PathVariable long deviceId)
    {
        Device device = deviceService.update(deviceId, updateDevice);

        syncService.syncMonitoringDatabase(device, "update");

        return ResponseEntity.ok(device);
    }

    @PostMapping("/removeOwner/{deviceId}")
    public ResponseEntity<?> removeOwner(@PathVariable long deviceId)
    {
        Device device = deviceService.getDevice(deviceId);
        deviceService.removeOwner(deviceId);

        syncService.syncMonitoringDatabase(device, "removeOwner");

        return ResponseEntity.ok(HttpEntity.EMPTY);
    }

    @PostMapping("/deviceLink")
    public ResponseEntity<?> deviceLink(@RequestBody LinkDeviceRequest request)
    {
        Device linkedDevice = deviceService.linkDevice(request);

        syncService.syncMonitoringDatabase(linkedDevice, "link");

        return ResponseEntity.ok(linkedDevice);
    }

    @GetMapping("/getUserDevices/{userGuid}")
    public ResponseEntity<?> getUserDevices(@PathVariable String userGuid)
    {
        List<Device> devices = deviceService.getAllUserDevices(userGuid);

        return ResponseEntity.ok(devices);
    }
}
