package com.example.deviceservice.Services;

import com.example.deviceservice.Producers.Models.SyncDevice;
import com.example.deviceservice.Producers.RabbitMQProducer;
import com.example.deviceservice.Repository.Models.Device;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@RequiredArgsConstructor
public class SynchronizationService {
    private final RabbitMQProducer syncProducer;
    private final DeviceService deviceService;
    @PostConstruct
    public void startUpSynchronization(){
        List<Device> devices = deviceService.getAllDevice();

        for(Device device: devices){
            SyncDevice convertedDevice = convertDevice(device, "check");
            syncProducer.send(convertedDevice);
        }
    }

    public void syncMonitoringDatabase(List<Device> devices, String syncType){
        for(Device device: devices){
            SyncDevice convertedDevice = convertDevice(device, syncType);
            syncProducer.send(convertedDevice);
        }
    }

    public void syncMonitoringDatabase(Device device, String syncType){
        SyncDevice convertedDevice = convertDevice(device, syncType);
        syncProducer.send(convertedDevice);
    }

    private SyncDevice convertDevice(Device device, String syncType){
        return new SyncDevice(
                device.getGuid(),
                device.getOwnerGuid(),
                device.getEnergyConsumption(),
                syncType
        );
    }

}
