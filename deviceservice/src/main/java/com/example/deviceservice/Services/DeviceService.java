package com.example.deviceservice.Services;

import com.example.deviceservice.Controllers.Models.LinkDeviceRequest;
import com.example.deviceservice.Repository.IDeviceRepository;
import com.example.deviceservice.Repository.Models.Device;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DeviceService {
    private final IDeviceRepository deviceRepository;
    public Device getDevice(long id){
        return deviceRepository.findById(id).orElseThrow(
                ()-> new RuntimeException("Cannot find the user with the guid: " + id));
    }

    public List<Device> getAllDevice(){
       return deviceRepository.findAll();
    }

    public List<Device> getAllUserDevices(String ownerGuid){
        return deviceRepository.findAllUserDevices(ownerGuid).orElseThrow(
                ()-> new RuntimeException("could not get the devices for user "+ ownerGuid)
        );
    }

    public void deleteDevice(long id){
        deviceRepository.deleteById(id);
    }

    public List<Device> deleteUserDevices(String userGuid){
        List<Device> devices = deviceRepository.findAllUserDevices(userGuid).orElse(null);

        if(devices != null){
            for (Device removeLinkDevice : devices) {
                removeLinkDevice.setOwnerGuid(null);
                deviceRepository.save(removeLinkDevice);
            }
        }

        return devices;
    }

    public Device save(Device newDevice){
        return deviceRepository.save(newDevice);
    }

    public Device update(long deviceId, Device updatedDevice){
        Device device = deviceRepository.findById(deviceId).orElseThrow(
                () -> new RuntimeException("Cannot find the device to update with the guid: " + updatedDevice.getId()));

        device.setName(updatedDevice.getName());
        device.setDescription(updatedDevice.getDescription());
        device.setEnergyConsumption(updatedDevice.getEnergyConsumption());
        device.setOwnerGuid(updatedDevice.getOwnerGuid());
        device.setAddress(updatedDevice.getAddress());

        return deviceRepository.save(device);
    }

    public void removeOwner(long deviceId){
        Device device = deviceRepository.findById(deviceId).orElseThrow(
                () -> new RuntimeException("Cannot find the device to update with the guid: " + deviceId));
        device.setOwnerGuid(null);

        deviceRepository.save(device);
    }

    public Device linkDevice(LinkDeviceRequest request){
        Device device = deviceRepository.findById(request.getDeviceId()).orElseThrow(
                () -> new RuntimeException("Cannot find the device to link with the guid: " + request.getDeviceId()));
        device.setOwnerGuid(request.getOwnerGuid());
        return deviceRepository.save(device);
    }
}
