package com.example.chatservice.handler;

import com.example.chatservice.model.ChatMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDate;

@Controller
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/sockets")
@RequiredArgsConstructor
public class ChatHandler {
    final SimpMessagingTemplate simpMessageTemplate;



    @GetMapping("/get")
    public ResponseEntity<?> mesgTest(){
        ChatMessage message = new ChatMessage();
        long time = System.currentTimeMillis();
        message.setTimestamp(time);
        message.setSendFrom("Admin");
        message.setSendTo("8ef7328c-bf64-4994-8299-f4c57899fdd9");
        message.setMessage("salut test");
        message.setType("message");

        simpMessageTemplate.convertAndSendToUser("302c615d-0554-4b9b-bd7c-2d62332715a8", "/message", message);
        return ResponseEntity.ok(message);
    }

    @GetMapping("/getAdmin")
    public ResponseEntity<?> mesgAdminTest(){
        ChatMessage message = new ChatMessage();
        long time = System.currentTimeMillis();
        message.setTimestamp(time);
        message.setSendFrom("Admin");
        message.setSendTo("8ef7328c-bf64-4994-8299-f4c57899fdd9");
        message.setMessage("salut test");
        message.setType("message");

        simpMessageTemplate.convertAndSend("/chat/message/admin", message);
        return ResponseEntity.ok(message);
    }

    @MessageMapping("/chat/message/admin")
    public ChatMessage sendMessageToAdmin(@Payload ChatMessage message){
        simpMessageTemplate.convertAndSend("/chat/message/admin", message);
        return message;
    }

    @MessageMapping("/chat/{userGuid}/message")
    public ChatMessage sendMessageToUser(@Payload ChatMessage message, @DestinationVariable("userGuid") String userGuid){
       simpMessageTemplate.convertAndSendToUser(message.sendTo, "/message", message);
       return message;
    }
}
