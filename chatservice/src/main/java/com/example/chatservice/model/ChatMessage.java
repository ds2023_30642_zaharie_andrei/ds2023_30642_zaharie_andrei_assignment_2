package com.example.chatservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class ChatMessage {
    public String type;
    public String message;
    public String sendFrom;
    public String sendTo;
    private long timestamp;

    public ChatMessage() {

    }
}
